// 这里放 axios 的配置

// 1. 全局配置  请求根路径
// https://www.axios-http.cn/docs/config_defaults
axios.defaults.baseURL = 'http://www.itcbc.com:8000';


// 2.全局配置 Authorization 请求头
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
axios.defaults.headers.common['Authorization'] = localStorage.getItem('token');