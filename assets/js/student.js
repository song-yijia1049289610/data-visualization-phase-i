// ----------------查询所有学员,循环展示-----------------
function renderStudent() {
  axios.get('/student/list').then(({ data: res }) => {
    let { code, data } = res;
    if (code === 0) {
      let trs = '';
      data.forEach(item => {
        trs += `
          <tr>
            <th scope="row">${item.id}</th>
            <td>${item.name}</td>
            <td>${item.age}</td>
            <td>${item.sex}</td>
            <td>${item.group}</td>
            <td>${item.phone}</td>
            <td>${item.salary}</td>
            <td>${item.truesalary}</td>
            <td>${item.province} ${item.city} ${item.county}</td>
            <td>
            <button data-id="${item.id}" data-province="${item.province}" type="button" class="btn btn-primary btn-sm edit">修改</button>
              <button  data-id="${item.id}" type="button" class="btn btn-danger btn-sm del">删除</button>
            </td>
          </tr>
          `;
      });
      $('tbody').html(trs);
    }
  });
}
renderStudent();

// ---------------------删除学员---------------------
$('tbody').on('click', '.del', function () {
  if (!confirm('确定要删除吗?')) return
  // 获取id: data() 方法是jQuery封装的方法。作用是获取元素的 data-xxx 属性值
  let id = $(this).data('id')
  // console.log(id);
  axios.delete('/student/delete', { params: { id } }).then(result => {
    // console.log(result);
    let { code, message } = result.data
    if (code === 0) {
      // 提示:
      toastr.success(message);
      // 重新调用函数，重新获取全部学员信息，重新渲染到页面
      renderStudent()
    }
  })
})



// --------------------省市县联动--------------------

// 封装循环遍历数据的函数:
function renderGeo(res, cnName, enName) {
  let str = `<option selected value="">--${cnName}--</option>`
  res.forEach(item => {
    str += `<option value="${item}">${item}</option>`
  })
  $(`select[name=${enName}]`).html(str)
}




// 1. 页面加载完毕之后,马上获取全部的省
axios.get('/geo/province').then(({ data: res }) => {
  renderGeo(res, '省', 'province')
})


// 2. 省切换的时候,获取对应的市
$('select[name=province]').on('change', function () {
  // 获取当前选择的省
  // 对于下拉框来说, $(select).val() 找的并不是 select 标签的value属性,而是 option 的value属性
  let pname = $(this).val()

  // 省切换的时候，把县恢复原样
  $('select[name=county]').html('<option selected value="">--县--</option>')

  // 判断: 如果选择了空值,则清空市和县,同时return
  if (pname === '') {
    $('select[name=city]').html('<option selected value="">--市--</option>')
    return
  }

  axios.get('/geo/city', { params: { pname } }).then(({ data: res }) => {
    renderGeo(res, '市', 'city')
  })
})



// 3. 市切换的时候,获取对应的县
$('select[name=city]').on('change', function () {
  // 获取当前选择的省
  let pname = $(this).parent().prev().children().eq(0).val()
  // 获取当前选择的市
  let cname = $(this).val()
  // console.log(pname);

  // 判断: 如果选择了空值,则清空县,同时return
  // 左右结构,右边先执行
  if (cname === '') return $('select[name=county]').html('<option selected value="">--县--</option>')


  axios.get('/geo/county', { params: { pname, cname } }).then(({ data: res }) => {
    renderGeo(res, '县', 'county')
  })
})









// ----------------------添加学员-----------------------
// 1. 研究弹框是怎么回事? 去 https://v5.bootcss.com/docs/components/modal/  复制的
// 2. 省市县联动
// 3. 表单验证
function test() {
  return {
    fields: {
      name: { // 这里 name 是 input 的name属性值，表示对这个输入框进行验证
        validators: {
          notEmpty: {   //不能为空
            message: '姓名不能为空.'
          },
          stringLength: {   //检测长度
            min: 2,
            max: 10,
            message: '姓名需要2~10个字符'
          }
        }
      },
      age: {
        validators: {
          notEmpty: {
            message: '年龄不能为空'
          },
          greaterThan: {
            value: 18,
            message: '年龄必须大于等于18'
          },
          lessThan: {
            value: 50,
            message: '年龄必须小于等于50'
          }
        }
      },
      group: {
        validators: {
          notEmpty: {
            message: '组号不能为空'
          },
          greaterThan: {
            value: 1,
            message: '组号必须大于等于1'
          },
          lessThan: {
            value: 20,
            message: '组号必须小于等于20'
          }
        }
      },
      sex: {
        validators: {
          notEmpty: {
            message: '性别不能为空'
          },
          regexp: {
            regexp: /^男|女$/i,
            message: '性别只能是男或者女'
          }
        }
      },
      phone: {
        validators: {
          notEmpty: {
            message: '手机号不能为空'
          },
          regexp: {
            regexp: /^1\d{10}$/i,
            message: '手机号格式不正确'
          }
        }
      },
      salary: {
        validators: {
          notEmpty: {
            message: '期望薪资不能为空'
          },
          greaterThan: {
            value: 100,
            message: '期望薪资必须大于等于100'
          },
          lessThan: {
            value: 99999,
            message: '期望薪资必须小于等于99999'
          }
        }
      },
      truesalary: {
        validators: {
          notEmpty: {
            message: '实际薪资不能为空'
          },
          greaterThan: {
            value: 100,
            message: '实际薪资必须大于等于100'
          },
          lessThan: {
            value: 99999,
            message: '实际薪资必须小于等于99999'
          }
        }
      },
      province: {
        validators: {
          notEmpty: {
            message: '省不能为空'
          },
          stringLength: {   // 检测长度
            min: 1,
            max: 20,
            message: '省不能为空'
          }
        }
      },
      city: {
        validators: {
          notEmpty: {
            message: '市不能为空'
          },
          stringLength: {   //检测长度
            min: 1,
            max: 20,
            message: '市不能为空'
          }
        }
      },
      county: {
        validators: {
          notEmpty: {
            message: '县不能为空'
          },
          stringLength: {   //检测长度
            min: 1,
            max: 20,
            message: '县不能为空'
          }
        }
      }
    }
  }
}


// 4. 表单提交,完成添加
$('#addModal form').bootstrapValidator(test()).on('success.form.bv', function (e) {
  e.preventDefault();
  // 通过验证，这里的代码将会执行。我们将Ajax请求的代码放到这里即可
  // console.log(123);
  let data = $(this).serialize();
  axios.post('/student/add', data).then(({ data: res }) => {
    if (res.code === 0) {
      toastr.success(res.message); // 提示
      renderStudent(); // 更新页面数据
      $('#addModal').modal('hide'); // 关闭bootstrap的模态框   https://v4.bootcss.com/docs/components/modal/#modalshow
    }
  })
});


// -----------------------修改学员-------------------
// 点击修改按钮
$('tbody').on('click', '.edit', function () {
  let id = $(this).data('id');
  // 先做数据回填（所有的修改操作，输入框不能是空的，必须把数据的默认值填好）
  axios.get('/student/one', { params: { id } }).then(({ data: res }) => {
    let { code, data } = res;
    if (code === 0) {
      // console.log(data);
      // 设置每个表单项的默认值
      $('#updateModal input[name=id]').val(data.id);
      $('#updateModal input[name=age]').val(data.age);
      $('#updateModal input[name=phone]').val(data.phone);
      $('#updateModal input[name=salary]').val(data.salary);
      $('#updateModal input[name=truesalary]').val(data.truesalary);

      // $('input[属性=值][属性=值]')  属性选择器，可以使用多个属性；多个属性之间是并且关系
      $(`#updateModal input[name=sex][value=${data.sex}]`).prop('checked', true); // prop方法作用是设置input勾选

      // 对于下拉框来说，设置value，即可选中该项
      $('#updateModal select[name=group]').val(data.group);
      $('#updateModal select[name=province]').val(data.province);
      $('#updateModal select[name=city]').append(`<option value="${data.city}" selected>${data.city}</option>`);
      $('#updateModal select[name=county]').append(`<option value="${data.county}" selected>${data.county}</option>`);
    }
  });
  $('#updateModal').modal('show'); // 让模态框显示
})

// 表单提交,完成最后的修改
$('#updateModal form').bootstrapValidator(test()).on('success.form.bv',function (e) {
  e.preventDefault()
  // 通过验证，这里的代码将会执行。我们将Ajax请求的代码放到这里即可
  let data = $(this).serialize()
  axios.put('/student/update',data).then( ({data:res}) => {
    if (res.code === 0) {
      toastr.success(res.message); // 提示
      renderStudent() // 更新页面数据
      $('#updateModal').modal('hide') // 关闭弹出层
    }
  })
})