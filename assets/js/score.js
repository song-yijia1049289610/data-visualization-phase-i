// ----------获取全部的5次成绩--------
axios.get('/score/list').then(({ data: res }) => {
    let { code, data } = res
    // 判断:
    if (code === 0) {
        let str = ''
        for (let key in data) {
            // key 就是对象的键，表示学员id
            // data[key] 表示对象的值
            str += `
            <tr>
               <th scope="row">${key}</th>
               <td>${data[key].name}</td>
               <td class="score" data-id="${key}" data-batch="1">${data[key].score[0]}</td>
               <td class="score" data-id="${key}" data-batch="2">${data[key].score[1]}</td>
               <td class="score" data-id="${key}" data-batch="3">${data[key].score[2]}</td>
               <td class="score" data-id="${key}" data-batch="4">${data[key].score[3]}</td>
               <td class="score" data-id="${key}" data-batch="5">${data[key].score[4]}</td>
            </tr>
            `
        }
        // 循环结束,会得到很多tr,将这些tr放到父元素tbody里面
        $('tbody').html(str)
    }
})


// -----------点击单元格,哪个编辑数据-------------
// 思路：点击 td 的时候，创建input，把input放到 td 中。
$('tbody').on('click', '.score', function () {
    let td = $(this);
    if (td.children().length > 0) return; // 判断td中的子元素的个数，如果大于0。则表示有一个input了。则 return
    // 获取td中的 分数
    let text = td.text();
    // 清空td
    td.html('');
    // 创建input，并放到td中
    let input = $('<input />');
    // 美化input
    input.css({
        border: 'none', // 边框隐藏
        outline: 'none', // 轮廓隐藏
        background: td.css('background'), // 背景颜色
        textAlign: 'center' // 文字居中
    });
    input.val(text);
    td.append(input)
    input.trigger('focus'); // 让input获取焦点

    // -------------------- 注册 blur(失去焦点) 事件 ----------------------
    input.on('blur', function () {
        // 失去焦点，表示取消修改或录入
        // 把原来的值，放回到 td 中
        td.text(text);
    })

    // --------------------注册 keyup(键盘按下) 事件-------------
    input.on('keyup', function (e) {
        if (e.key === 'Enter') {
            // 按了回车键，表示确认修改
            // 向接口发送请求，提交数据，完成真正的修改
            let data = {
                stu_id: td.data('id'), //学员id,
                batch: td.data('batch'), // 次数,
                score: $(this).val(), // 分数
            };
            // console.log(data);
            axios.post('/score/entry', data).then(({ data: res }) => {
                if (res.code === 0) {
                    toastr.success(res.message); // 提示
                    td.text(data.score); // 设置新的分数
                }
            });
        }
    })
})

