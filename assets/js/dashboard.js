// ------------- 饼图 --------------
// 封装一个函数
function pieChart(pie) {
    let myChart = echarts.init($('.pie')[0]);
    // $('.pie')是一个jQuery对象,需要加一个[0]转成DOM对象
    let option = {
        // 标题
        title: {
            text: '籍贯 Hometown',
            textStyle: {
                color: '#6d767e'
            }
        },
        // 鼠标移入的提示
        tooltip: {
            // 自定义提示内容
            formatter: '{a}<br />{b} <strong>{c}</strong>人 占比{d}%'
        },
        // 系列数据:
        series: [
            {
                name: '各地人员分布',
                type: 'pie', // pie 表示饼图
                radius: ['10%', '70%'], // 半径
                center: ['50%', '50%'], // 中心点坐标
                // roseType 表示图表的扇形区域如何展示数据,可选值有两个(area和radius)
                // area 表示使用扇形的半径表示数据的大小,每个扇形的圆心角相同
                // radius 表示使用扇形的半径表示数据的大小,使用圆心角表示占比
                roseType: 'area',
                // itemStyle 表示每一项的样式
                itemStyle: {
                    // borderRadius 扇形圆角设置
                    borderRadius: 6
                },
                // data 表示的是数据部分
                data: pie
            }
        ]
    };
    myChart.setOption(option);
}
// pieChart()


// ------------- 折线图 -------------
function lineChart(lineName,salary,truesalary) {
    let myChart = echarts.init($('.line')[0]);
    let option = {
        // 图例
        legend: {
            top: 20
        },
        // 鼠标移入的提示
        tooltip: {
            // axis表示轴触发，鼠标放到线条上面或下面也可以触发
            // item 表示鼠标放到每一项上才能触发
            trigger: 'axis', // 触发方式
            // 悬浮框的位置
            position: function (pt) {
                // pt = [鼠标x轴位置, 鼠标y轴位置]
                return [pt[0], '15%']; // 悬浮框左右方向跟随鼠标移动；上下方向不变（始终距离顶部15%）
            }
        },
        // 标题
        title: {
            text: '薪资 Salary',
            textStyle: {
                color: '#6d767e'
            }
        },
        // 工具箱（没用）
        // toolbox: {
        //   feature: {
        //     dataZoom: {
        //       yAxisIndex: 'none'
        //     },
        //     restore: {},
        //     saveAsImage: {}
        //   }
        // },
        xAxis: {
            type: 'category',
            // 坐标轴留白策略
            boundaryGap: false, // true表示留白；false表示不留白
            // data: date
            data: lineName,
        },
        yAxis: {
            type: 'value',
            // 坐标轴留白策略
            // 数组，表示数据的延伸
            // y轴数据最大值 ==== 数据最大值 + 数据最大值 * 延伸比例  

            // y轴数据最大值 ==== 17000 + 17000 * 50% === 25500
            boundaryGap: [0, '50%']
        },
        // 数据缩放组件
        // 数据缩放组件，有三种类型
        // type: select 通过工具箱来缩放
        // type: inside 会把数据缩放组件内置到x轴上，通过鼠标滚轮缩放
        // type: slider (默认值)，会有一个单独的拖动条
        dataZoom: [
            {
                // type: 'slider',
                start: 0, // start和end都是百分比
                end: 15
            }
        ],
        // 系列数据
        series: [
            {
                name: '期望薪资',
                type: 'line',
                smooth: true, // 使用平滑曲线
                symbol: 'none', // 设置线条上没有任何点，也可以设置为空心圆、实心圆、矩形、.......
                itemStyle: {
                    color: 'rgb(255, 70, 131)'
                },
                // areaStyle: {}, // 表示面试的这项删除
                data: salary
            },
            {
                name: '实际薪资',
                type: 'line',
                smooth: true,
                symbol: 'none',
                itemStyle: {
                    color: 'rgb(255, 200, 31)'
                },
                data: truesalary
            }
        ]
    };
    myChart.setOption(option);
}
// lineChart()



// ------------- 柱状图 -------------
// let 形参 = 实参
// let { avgScore, group, gt60, gt80, lt60 } = 实参
function barChart({ group, avgScore, lt60, gt60, gt80 }) {
    let myChart = echarts.init($('.barChart')[0])
    let option = {
        tooltip: {
            trigger: 'axis',  // 轴触发 可选 item(每一项上触发)
            // 坐标轴指示器:
            axisPointer: {
                type: 'cross', // 控制横向的的那条虚线
                crossStyle: {
                    color: '#999'
                }
            }
        },
        // 网格
        grid: {
            top: 30,
            bottom: 30,
            left: '7%',
            right: '7%'
        },
        // 图例
        legend: {},
        xAxis: [
            {
                type: 'category',
                data: group,
                axisPointer: {
                    type: 'shadow' // 阴影
                }
            }
        ],
        yAxis: [
            {
                type: 'value',
                // name: '分数',
                min: 0,
                max: 100,
                interval: 10,
                axisLabel: {
                    formatter: '{value} 分'
                }
            },
            {
                type: 'value',
                // name: '人数',
                min: 0,
                max: 10,
                interval: 1,
                axisLabel: {
                    formatter: '{value} 人'
                }
            }
        ],
        series: [
            {
                name: '平均分',
                type: 'bar',
                barWidth: 15,
                data: avgScore
            },
            {
                name: '低于60分人数',
                // 指定y轴的索引(使用哪条y轴)
                yAxisIndex: 1,
                type: 'bar',
                barWidth: 15,
                data: lt60
            },
            {
                name: '60到80分人数',
                type: 'bar',
                barWidth: 15,
                yAxisIndex: 1,
                data: gt60
            },
            {
                name: '高于80分人数',
                type: 'bar',
                barWidth: 15,
                yAxisIndex: 1,
                data: gt80
            }
        ]
    };
    myChart.setOption(option)
}
// barChart()

// ------------- 地图 --------------
function mapChart(chinaDatas,chinaGeoCoordMap) {
    let myChart = echarts.init($('.map')[0])
    var chinaGeoCoordMap = chinaGeoCoordMap
    var chinaDatas = chinaDatas

    var convertData = function (data) {
        var res = [];
        for (var i = 0; i < data.length; i++) {
            var dataItem = data[i];
            var fromCoord = chinaGeoCoordMap[dataItem[0].name];
            var toCoord = [116.4551, 40.2539];
            if (fromCoord && toCoord) {
                res.push([{
                    coord: fromCoord,
                    value: dataItem[0].value
                }, {
                    coord: toCoord,
                }]);
            }
        }
        return res;
    };
    var series = [];
    [['北京市', chinaDatas]].forEach(function (item, i) {
        console.log(item)
        series.push({
            type: 'lines',
            zlevel: 2,
            effect: {
                show: true,
                period: 2, //箭头指向速度，值越小速度越快
                trailLength: 0.02, //特效尾迹长度[0,1]值越大，尾迹越长重
                symbol: 'arrow', //箭头图标
                symbolSize: 5, //图标大小
            },
            lineStyle: {
                normal: {
                    width: 1, //尾迹线条宽度
                    opacity: 1, //尾迹线条透明度
                    curveness: .3 //尾迹线条曲直度
                }
            },
            data: convertData(item[1])
        }, {
            type: 'effectScatter',
            coordinateSystem: 'geo',
            zlevel: 2,
            rippleEffect: { //涟漪特效
                period: 4, //动画时间，值越小速度越快
                brushType: 'stroke', //波纹绘制方式 stroke, fill
                scale: 4 //波纹圆环最大限制，值越大波纹越大
            },
            label: {
                normal: {
                    show: true,
                    position: 'right', //显示位置
                    offset: [5, 0], //偏移设置
                    formatter: function (params) {//圆环显示文字
                        return params.data.name;
                    },
                    fontSize: 13
                },
                emphasis: {
                    show: true
                }
            },
            symbol: 'circle',
            symbolSize: function (val) {
                return 5 + val[2] * 5; //圆环大小
            },
            itemStyle: {
                normal: {
                    show: false,
                    color: '#f00'
                }
            },
            data: item[1].map(function (dataItem) {
                return {
                    name: dataItem[0].name,
                    value: chinaGeoCoordMap[dataItem[0].name].concat([dataItem[0].value])
                };
            }),
        },
            //被攻击点
            {
                type: 'scatter',
                coordinateSystem: 'geo',
                zlevel: 2,
                rippleEffect: {
                    period: 4,
                    brushType: 'stroke',
                    scale: 4
                },
                label: {
                    normal: {
                        show: true,
                        position: 'right',
                        //offset:[5, 0],
                        color: '#0f0',
                        formatter: '{b}',
                        textStyle: {
                            color: "#0f0"
                        }
                    },
                    emphasis: {
                        show: true,
                        color: "#f60"
                    }
                },
                symbol: 'pin',
                symbolSize: 50,
                data: [{
                    name: item[0],
                    value: chinaGeoCoordMap[item[0]].concat([10]),
                }],
            }
        );
    });

    let option = {
        // 标题:
        title: {
            text: '来京路线 From',
            textStyle: {
                color: '#6d767e'
            }
        },
        tooltip: {
            trigger: 'item',
            backgroundColor: 'rgba(166, 200, 76, 0.82)',
            borderColor: '#FFFFCC',
            showDelay: 0,
            hideDelay: 0,
            enterable: true,
            transitionDuration: 0,
            extraCssText: 'z-index:100',
            formatter: function (params, ticket, callback) {
                //根据业务自己拓展要显示的内容
                var res = "";
                var name = params.name;
                var value = params.value[params.seriesIndex + 1];
                res = "<span style='color:#fff;'>" + name + "</span><br/>数据：" + value;
                return res;
            }
        },
        backgroundColor: "#fff",
        // 视觉映射组件:
        visualMap: { //图例值控制
            show: false,
            min: 0,
            max: 1,
            calculable: true,
            // show: true,
            color: ['#f44336', '#fc9700', '#ffde00', '#ffde00', '#00eaff'],
            textStyle: {
                color: '#fff'
            }
        },
        geo: {
            map: 'china',
            zoom: 1.2,
            label: {
                emphasis: {
                    show: false
                }
            },
            roam: true, //是否允许缩放
            itemStyle: {
                normal: {
                    color: 'rgba(51, 69, 89, .5)', //地图背景色
                    borderColor: '#516a89', //省市边界线00fcff 516a89
                    borderWidth: 1
                },
                emphasis: {
                    color: 'rgba(37, 43, 61, .5)' //悬浮背景
                }
            }
        },
        series: series
    };
    myChart.setOption(option)
}
// mapChart()




// ---------------------Ajax获取数据---------------

// 1. 发送请求,获取班级概况数据
axios.get('/student/overview').then(({ data: res }) => {
    if (res.code === 0) {
        // console.log(res.data);
        $('.total').text(res.data.total)
        $('.avgSalary').text(res.data.avgSalary)
        $('.avgAge').text(res.data.avgAge)
        $('.proportion').text(res.data.proportion)
    }
})

// 2. 柱状图
// 2.1 点击'三个点',切换下拉菜单
$('.bar .btn').on('click', function () {
    // $(this) 代表'三个点'
    // $(this).next() 代表它的下一个元素,就是下拉列表
    $(this).next().toggle()
})

// 2.2 点击li,获取对应的成绩,拿到成绩后,传入柱状图中使用
$('#batch > li').on('click', function () {
    let index = $(this).index() // 得到当前li的索引值(li元素在兄弟之间的位置)
    let batch = index + 1 // 得到考试的次数
    // 发送请求,获取柱状图所需数据
    axios.get('/score/batch', { params: { batch: batch } }).then(({ data: res }) => {
        // console.log(res);
        if (res.code === 0) {
            barChart(res.data)
        }
    })
})

// 2.3 点击第一个li,就会得到第一次成绩(页面刷新后,就得到第一次成绩)
// 页面刷新后 ====> 点击第1个li  =====>   得到第一次成绩
// 固定语法: $('元素').trigger('事件');
$('#batch > li').eq(0).trigger('click') // trigger是jQuery的方法,专门用于触发元素的事件



// 3. 获取全部学员的信息(从中提取 折线图所需的数据,饼图所需的数据,地图所需的数据)
axios.get('/student/list').then(({ data: res }) => {
    let { code, data } = res
    if (code === 0) {
        // 整理出 折线图所需的数据,饼图所需的数据,地图所需的数据
        // console.log(data);
        let lineName = [];
        let salary = [];
        let truesalary = [];

        // 饼图所需数据
        let pie = [];  // [{name: '河北省', value: 3}, {name: '内蒙古', value: 1}, {name: '山东省', value: 2}, {name: '辽宁省', value: 1}]

        // 地图所需数据
        let chinaGeoCoordMap = { '北京市': [116.4551, 40.2539] };
        let chinaDatas = [];

        // 循环遍历
        data.forEach(item => {

            // -------------折线图所需数据------------------ 
            lineName.push(item.name)
            salary.push(item.salary)
            truesalary.push(item.truesalary)


            // -------------饼图所需数据-------------------
            // 判断: pie这个数组中,是否已经有这个省了
            let i = pie.findIndex(v => v.name === item.province)
            // pie.findIndex(v => {
            //   return v.name === item.province
            // })

            // 找到就是索引,找不到就是-1
            if (i > -1) {
                pie[i].value += 1
            } else {
                pie.push({ name: item.province, value: 1 })
            }

            // -------------地图所需数据------------------
            chinaGeoCoordMap[item.province] = [item.jing, item.wei]
            // chinaDatas.push([{ name: item.province, value: 0 }])
            let k = chinaDatas.findIndex(v => v[0].name === item.province)
            // 判断:
            if (k > -1) {
                chinaDatas[k][0].value++
            } else {
                chinaDatas.push([{ name: item.province, value: 1 }])
            }
        })


        // 调用 三个图表的函数,传入数据
        lineChart(lineName,salary,truesalary)
        pieChart(pie)
        mapChart(chinaDatas,chinaGeoCoordMap)
    }
})