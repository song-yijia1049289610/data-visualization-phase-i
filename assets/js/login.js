// ----------切换两个盒子----------
$('.box a').on('click', function () {
    // 这里的this指 a
    // $(this).parents('.box') 指当前的盒子
    $(this).parents('.box').hide().siblings().show()
})

// -----------表单验证---------
// 要验证 username 不能为空 长度 2-10位
// 要验证 password 不能为空 长度 6-15位
// 比如，验证一个用户名和密码
function test() {
    return {
        fields: {
            username: { // 这里username是 input 的name属性值，表示对这个输入框进行验证
                validators: {
                    notEmpty: {   //不能为空
                        message: '用户名不能为空.'
                    },
                    stringLength: {   //检测长度
                        min: 2,
                        max: 15,
                        message: '用户名需要2~15个字符'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: '密码不能为空'
                    },
                    stringLength: {   //检测长度
                        min: 6,
                        max: 15,
                        message: '密码需要6~15个字符'
                    }
                }
            }
        }
    }
}





// -----------注册------------
// 前端: 按照接口要求,提交username和password
$('.register form').bootstrapValidator(test()).on('success.form.bv', function (e) {
    e.preventDefault();
    // 通过验证，这里的代码将会执行。我们将Ajax请求的代码放到这里即可
    // console.log('通过验证,注册');

    // 'username=xxx&password=yyy'
    let data = $(this).serialize()
    // console.log(data);
    axios.post('/api/register',data).then( ({data:result}) => {
        // console.log(result.data); // 真实的服务器响应的结果
        if (result.code === 0) { // 注册成功
            // 提示
            toastr.success(result.message)
            // 返回到登录,显示登录的盒子
            $('.login').show().siblings().hide()
            // 清空输入框
            $('.register input').val('')
        }
    })
});


// -----------登录------------
// 前端: 按照接口要求,提交username和password
$('.login form').bootstrapValidator(test()).on('success.form.bv', function (e) {
    e.preventDefault();
    // 通过验证，这里的代码将会执行。我们将Ajax请求的代码放到这里即可
    // console.log('通过验证,登录');
    let data = $(this).serialize()
    axios.post('/api/login',data).then(result => {
        // console.log(result.code);
        if (result.data.code === 0) {
            // 登录成功,页面跳转,存储token
            // localStorage.setItem('名',值)
            localStorage.setItem('token',result.data.token)
            location.href = './index.html'
        }
    })
});