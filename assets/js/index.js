/**
 * 侧边导航关闭折叠控制
 */

function toggleSlide() {
  $('.nav > li > a').on('click', function () {
    let childMenu = $(this).next('ul');
    childMenu.slideToggle(400);
    let icon = childMenu.prev().find('.toggle');
    if (icon.hasClass('open')) {
      icon.removeClass('open').addClass('close');
    } else {
      icon.removeClass('close').addClass('open');
    }
  })

  // 默认第一个菜单展开
  $('.nav > li > a').eq(0).trigger('click');

  // 所有子菜单切换时加背景色
  $('.nav ul a').on('click', function () {
    $(this).addClass('active')
    $('.nav ul a').not($(this)).removeClass('active');
  })

}

toggleSlide();




// ----------1.退出-----------
// 找到 退出 注册点击事件

$('.logout > a').on('click', function () {
  // 询问是否退出?
  if (!confirm('你确定退出吗')) return
  //  1. 清除token 2. 跳转到 login.html
  localStorage.removeItem('token')
  location.href = './login.html'
})



// ----------2.初始化数据-----------
// 说明: 为了方便开发,登陆后点击页面顶部的"点我初始化数据"的按钮
// 调用此接口将为你随机添加56名同学，并分为8个组。并为每位同学随机添加了三次成绩。
// 不允许重复初始化,初始化之后,再次初始化会提示已经初始化过了
// 测试时,可以注册使用新账号来测试

$('.init').on('click', function () {
  axios.get('/init/data').then(({ data: res }) => {
    if (res.code === 0) {
      // console.log('初始化成功');
      toastr.success(res.message) // 成功以后的提示
    }
  })
})


// -----------3. 添加响应拦截器------------
axios.interceptors.response.use(
  function (response) {
    // 2xx 范围内的状态码都会触发该函数。
    // 对响应数据做点什么
    // response 相当于 then函数中的 result
    // response.data 表示响应结果
    let { code, message } = response.data;
    if (code === 1) {
      toastr.warning(message);
    }
    return response;
  },
  function (error) {
    // 超出 2xx 范围的状态码都会触发该函数。
    // 对响应错误做点什么
    // error.response 相当于  result
    if (error.response) { // 在得到响应结果的前提之下，再做后续的处理
      let { code, message } = error.response.data;
     
      if(code===1&&message==='身份认证失败') {
        // 符合这个条件,说明以后使用了假token或者过期token
        localStorage.removeItem('token') // 移除过期的,或者伪造的假token
        location.href = './login.html' // 跳转到登录页
      }



      if (code === 1) {
        toastr.error(message);
      }
    }
    return Promise.reject(error);
  }
);